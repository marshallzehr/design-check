# Overview

You will design an _interactive prototype_ of a tool for an insurer that documents the completion of their
underwriting process. Underwriting happens before an insurance policy is issued.

Insurance is a complex business; we will stick to a simplified version here. Our focus will be on the process,
not the details of each underwriting step. Use a typical residential insurance policy as your scenario, how an
insurance application application is turned into the final decision to deny or approve.

You do not need to be a insurance expert to complete this project.  Its core elements are chared with many other
businesses you may be familliar with (manufacturing, aviation, mortgage lending); you can draw on that experience
as you do your design.

## Evaluation

There is no right answer; there are many possible good designs.  We simply seek to understand how you transform a
business problem into a functional design. We will do this in three parts:

1. "Hallway" usability test - can we successfully "use" your design?
2. Analytical review - look at whether you met all the requirements, how you solved the problem, how clearly your
   prototype communicates your design intent, how your design is organized, and how easy it was to test your design
3. Design process walkthrough - at your interview you will have the opportunity to walk us through the design process
   that led to your final design

## Background

Insurance underwriting is the process through which an insurer determines the riskiness of a potential insurance policy.
A risky policy is one for which the insurer has a high(er) likelihood of paying out more than the insurance premiums
they receive.

This process is a series of discrete analyses performed on the homeowner or the property. For example, an insurer may
check the credit score of the homeowner, the assessed value of the property, whether it is located in a flood plain,
the distance to the closest fire hydrant and fire station, the crime rate of the neighbourhood, the total value of
the goods that the homeowner has stored within the property, and the registered owner of the property.

This takes a small team to complete; insurance underwriters, assessors, law clerks, accounts payable, and customer
service all could be involved.  The team can complete some of the process in parallel to underwrite more quickly; in
other cases, the team must wait for the outcome of an earlier step before starting a later one.

Every step is usually double-checked (verified) by another person of the same role; this not only confirms that the
step was done, but that it was done correctly.

The process starts with an insurance application; an active insurance policy depends on all steps being done and
verified.

Insurers that can complete this process quickly without error have an advantage - reduced labour costs and better
customer experience.  The completion of each step is documented for process improvement and future audits.

## Outcome

The envisioned tool will be used by members of the insurance underwriting team to document which steps they have
done or verified.  It will also be used to see which steps are incomplete (to follow up or decide what to work on
next).

Your task is to design the above tool.

## Design guidance

We are looking particularly at the interaction design of your solution and the interactivity of your prototype. We are
not assessing the visual design of your prototype, so if the visuals do not affect the functionality
they may safely be omitted.  We do not care how "pretty" it is - only that it is usable.

Your prototype may be built using any design tool or programming framework that enables you to demonstrate
interactivity.  Your choice of tool does not affect our assessment, nor will we be assessing how you built your
prototype.

Design for a desktop web browser. Mobile-friendly design is not necessary and will not be assessed.

The particular steps you put in your insurance underwriting process are unimportant, so long as they demonstrate the key
elements of your design.  We prefer plausible text over lorem ipsum, but it need not be technically accurate.

## Requirements

Your design should meet all of the following requirements:

* Clearly communicate your design intent, both to future users of the product and the developers who would implement it
* Allow marking steps as done or verified
* Show steps that are not done or need verification
* Show steps that are being worked on, and those not started
* Show steps that can be worked immediately, and which require earlier steps to be completed first
* Show a complete record of the underwriting process, for audit

## Not in scope

Do not go out of your way to support these things:

* Multiple insurance policies or switching between policies - your prototype can show only the view(s) relevant to a
  single policy
* The tasks/files needed to do/verify a step - your prototype should only track the step state
* Compatibility with mobile, tablet, or touch devices

## Submission

Please send us a link to your interactive prototype - we prefer a functional prototype link over sources, but will
accept the latter if that is more convenient for you.

If you created any sketches or other artifacts as part of your process, feel free to share them with us.  And if you
feel that providing notes will help us in understanding your solution, you may include them.

## Time

We suggest that you time-box your design to _no more than_ 4 hours; we want to see evidence of your design insight,
not a perfect design.  It is acceptable to set a shorter time-box; please note this when submitting and we will take
that into account in our assessment.

You can take as long as you need to submit – we understand people have jobs and lives, so do what fits with your
schedule.

## Next Steps

After you submit, we will review your design and get next steps out to you within 2 business days.  If you
do not hear from us by then, feel free to follow up.

The next step in our interview process is an in-person (or video) interview.

## Questions

If you are unsure about an element of this design check you should state your assumption and proceed.  We
will consider any notes you send during our evaluation.

If you believe you cannot proceed without an answer, you can email us and we will update this README to clarify.

Note that we are intentionally giving high-level guidance rather than detailed specification to allow you a lot of
freedom in your design.

## Copyright

© MarshallZehr Group Inc.  All Rights Reserved.
